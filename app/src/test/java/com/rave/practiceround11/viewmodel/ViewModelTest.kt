package com.rave.practiceround11.viewmodel

import com.rave.practiceround11.model.SoccerRepo
import com.rave.practiceround11.model.local.Match
import com.rave.practiceround11.util.CoroutinesTestExtension
import com.rave.practiceround11.util.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
internal class ViewModelTest {

    @RegisterExtension
    private val testExtension = CoroutinesTestExtension()
    private val repo: SoccerRepo = mockk()
    private val viewModel = SoccerViewModel(repo)

    @Test
    fun testGetMatches() = runTest(testExtension.dispatcher) {
        val expectedResult = listOf(
            Match(
                homeTeam = "Argentina",
                awayTeam = "USA"
            )
        )
        coEvery { repo.getMatches() } coAnswers { expectedResult }
        val result = viewModel.matches.value
        Assertions.assertEquals(expectedResult, result)
    }
}
