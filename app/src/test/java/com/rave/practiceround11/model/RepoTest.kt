package com.rave.practiceround11.model

import com.rave.practiceround11.model.local.Match
import com.rave.practiceround11.model.remote.APIService
import com.rave.practiceround11.model.remote.dtos.AwayTeamDTO
import com.rave.practiceround11.model.remote.dtos.HomeTeamDTO
import com.rave.practiceround11.model.remote.dtos.MatchDTO
import com.rave.practiceround11.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

internal class RepoTest {

    @RegisterExtension
    private val testExtension = CoroutinesTestExtension()
    private val service: APIService = mockk()
    private val repo = SoccerRepo(service)

    @Test
    fun testGetMatches() = runTest(testExtension.dispatcher) {
        val serviceResult = listOf(
            MatchDTO(
                homeTeam = HomeTeamDTO(
                    abbr = null,
                    alias = null,
                    id = null,
                    name = "Argentina",
                    shortName = null
                ),
                awayTeam = AwayTeamDTO(
                    abbr = null,
                    alias = null,
                    id = null,
                    name = "USA",
                    shortName = null
                ),
                competitionStage = null,
                date = null,
                id = null,
                state = null,
                type = null,
                venue = null
            )
        )
        coEvery { service.getMatches() } coAnswers { serviceResult }
        val expectedResult = listOf(
            Match(
                homeTeam = "Argentina",
                awayTeam = "USA"
            )
        )
        val result = repo.getMatches()
        Assertions.assertEquals(expectedResult, result)
    }
}
