package com.rave.practiceround11

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Main class for dagger/hilt.
 *
 * @constructor Create empty Soccer application
 */
@HiltAndroidApp
class SoccerApplication : Application()
