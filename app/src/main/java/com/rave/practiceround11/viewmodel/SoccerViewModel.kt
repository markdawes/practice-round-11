package com.rave.practiceround11.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.practiceround11.model.SoccerRepo
import com.rave.practiceround11.model.local.Match
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Soccer view model.
 *
 * @property repo
 * @constructor Create empty Soccer view model
 */
@HiltViewModel
class SoccerViewModel @Inject constructor(private val repo: SoccerRepo) : ViewModel() {
    private val _matches: MutableLiveData<List<Match>> = MutableLiveData()
    val matches: LiveData<List<Match>> get() = _matches

    init {
        getMatches()
    }

    /**
     * Get matches.
     *
     */
    private fun getMatches() = viewModelScope.launch {
        _matches.value = repo.getMatches()
    }
}
