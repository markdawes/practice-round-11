package com.rave.practiceround11.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class VenueDTO(
    val id: Int?,
    val name: String?
)
