package com.rave.practiceround11.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class CompetitionStageDTO(
    val competition: CompetitionDTO?,
    val leg: String?,
    val stage: String?
)
