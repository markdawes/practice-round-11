package com.rave.practiceround11.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class HomeTeamDTO(
    val abbr: String?,
    val alias: String?,
    val id: Int?,
    val name: String?,
    val shortName: String?
)
