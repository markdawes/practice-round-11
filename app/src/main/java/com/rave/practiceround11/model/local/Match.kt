package com.rave.practiceround11.model.local

/**
 * Match.
 *
 * @property homeTeam
 * @property awayTeam
 * @constructor Create empty Match
 */
data class Match(
    val homeTeam: String,
    val awayTeam: String
)
