package com.rave.practiceround11.model.remote

import com.rave.practiceround11.model.remote.dtos.MatchDTO
import retrofit2.http.GET

/**
 * Api service.
 *
 * @constructor Create empty A p i service
 */
interface APIService {

    @GET("test-task/fixtures.json")
    suspend fun getMatches(): List<MatchDTO>
}
