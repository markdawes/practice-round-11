package com.rave.practiceround11.model

import com.rave.practiceround11.model.local.Match
import com.rave.practiceround11.model.remote.APIService
import javax.inject.Inject

/**
 * Soccer repo.
 *
 * @property service
 * @constructor Create empty Soccer repo
 */
class SoccerRepo @Inject constructor(private val service: APIService) {

    /**
     * Get matches.
     *
     * @return
     */
    suspend fun getMatches(): List<Match> {
        val matchDTOs = service.getMatches()
        return matchDTOs.map {
            Match(
                homeTeam = it.homeTeam!!.name!!,
                awayTeam = it.awayTeam!!.name!!
            )
        }
    }
}
