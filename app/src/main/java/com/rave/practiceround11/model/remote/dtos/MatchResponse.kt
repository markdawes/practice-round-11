package com.rave.practiceround11.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
class MatchResponse : ArrayList<MatchDTO>()
