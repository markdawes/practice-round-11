package com.rave.practiceround11.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class CompetitionDTO(
    val id: Int?,
    val name: String?
)
