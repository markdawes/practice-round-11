package com.rave.practiceround11.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.practiceround11.databinding.ItemMatchBinding
import com.rave.practiceround11.model.local.Match

/**
 * Match adapter.
 *
 * @constructor Create empty Match adapter
 */
class MatchAdapter : RecyclerView.Adapter<MatchAdapter.MatchViewHolder>() {

    private var matchList: List<Match> = emptyList()

    /**
     * Match view holder.
     *
     * @property binding
     * @constructor Create empty Match view holder
     */
    inner class MatchViewHolder(private val binding: ItemMatchBinding) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Display match.
         *
         * @param match
         */
        fun displayMatch(match: Match) = with(binding) {
            tvMatch.text = "${match.homeTeam} versus ${match.awayTeam}"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MatchViewHolder(
            ItemMatchBinding.inflate(inflater, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.displayMatch(matchList[position])
    }

    override fun getItemCount(): Int {
        return matchList.size
    }

    /**
     * Set data.
     *
     * @param matches
     */
    fun setData(matches: List<Match>) {
        this.matchList = matches
        notifyDataSetChanged()
    }
}
